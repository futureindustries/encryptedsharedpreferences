A small library for Android that encrypts data written to SharedPreferences.
By default it uses Android ID + Device ID as key for AES encryption,
but it can be easily modified to use some other algorithm.

Copyright Future Industries Inc. <infuturewetrust.com>
