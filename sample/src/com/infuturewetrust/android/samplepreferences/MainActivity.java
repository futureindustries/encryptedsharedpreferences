package com.infuturewetrust.android.samplepreferences;

import com.infuturewetrust.android.encryptedpreferences.EncryptedSharedPrefs;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private static final String SETTINGS_NAME = "user_settings";
	
	private EditText mEditText;
	private TextView mClearText, mBase64;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mEditText = (EditText) findViewById(R.id.data);
		mClearText = (TextView) findViewById(R.id.clear_text);
		mBase64 = (TextView) findViewById(R.id.base64);
		
		findViewById(R.id.btnUpdate).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (mEditText.getText().toString().length() > 0) {
					EncryptedSharedPrefs.setString(SETTINGS_NAME, getBaseContext(), "data", mEditText.getText().toString());
					
					final SharedPreferences settings = getBaseContext().getSharedPreferences(SETTINGS_NAME, 0);
					String encrypted = settings.getString("data", null);
					mBase64.setText(encrypted);
					mClearText.setText(EncryptedSharedPrefs.getString(SETTINGS_NAME, getBaseContext(), "data"));
				} else {
					Toast.makeText(getBaseContext(), "Type some text", Toast.LENGTH_LONG).show();
				}
			}});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
