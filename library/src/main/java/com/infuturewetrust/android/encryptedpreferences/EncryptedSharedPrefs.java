package com.infuturewetrust.android.encryptedpreferences;

/*
Copyright 2012 Future Industries Inc.

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
*/

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;

/*
 * A wrapper around SharedPreferences which encrypts values
 * stored in the file and decrypts those when fetched.
 */

public class EncryptedSharedPrefs {
	
	private static String ENC_KEY;

	private EncryptedSharedPrefs() {
	}
	
	/**
	 * Uses (Android ID + Device ID) combination as an encryption key.
	 * @param Context Application context
	 * @return String Encryption key
	 */
	private static String getEncryptionKey(final Context context) {
		if (ENC_KEY == null) {
			final String androidId = Settings.Secure.ANDROID_ID;
			final String deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
			ENC_KEY = androidId + deviceId;
		}
		
		return ENC_KEY;
	}
	
	/**
	 * Save string to shared preferences
	 * @param String	Name of the preferences file
	 * @param Context	Application context
	 * @param String	Key
	 * @param String	Value
	 * @return boolean	Status of the write
	 */
	public static boolean setString(final String name, final Context context,
			final String key, final String value) {
		if (context == null || key == null || value == null) return false;
		final String v = Crypt.encryptString(getEncryptionKey(context), value);
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		final SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, v);
		return editor.commit();
	}
	
	/**
	 * Fetches string by provided key
	 * @param String	Name of the preferences file
	 * @param Context	Application context
	 * @param String	Key
	 * @return String	Decrypted value, or null if error
	 */
	public static String getString(final String name, final Context context, final String key) {
		if (context == null || key == null) return null;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		final String encrypted = settings.getString(key, null);
		
		return encrypted == null ? encrypted : Crypt.decryptString(getEncryptionKey(context), encrypted);
	}
	
	/**
	 * Save boolean value to shared preferences
	 * @param Context	Application context
	 * @param String	Key
	 * @param boolean	Value
	 * @return boolean	Status of the write
	 */
	public static boolean setBoolean(final String name, final Context context,
			final String key, final boolean value) {
		if (context == null || key == null) return false;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		final SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(key, value);
		return editor.commit();
	}
	
	/**
	 * Fetches boolean by provided key
	 * @param Context	Application context
	 * @param String	Key
	 * @return boolean	Decrypted value, or false if error
	 */
	public static boolean getBoolean(final String name,
			final Context context,
			final String key) {
		if (context == null || key == null) return false;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		return settings.getBoolean(key, false);
	}
	
	/**
	 * Save integer value to shared preferences
	 * @param Context	Application context
	 * @param String	Key
	 * @param int		Value
	 * @return boolean	Status of the write
	 */
	public static boolean setInteger(final String name, final Context context,
			final String key, final int value) {
		if (context == null || key == null) return false;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		final SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, value);
		return editor.commit();
	}
	
	/**
	 * Fetches integer by provided key
	 * @param Context	Application context
	 * @param String	Key
	 * @return int		Decrypted value, or Integer.MIN_VALUE if error.
	 */
	public static int getInteger(final String name, final Context context, final String key) {
		if (context == null || key == null) return 0;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		return settings.getInt(key, Integer.MIN_VALUE);
	}
	
	/**
	 * Save long to shared preferences
	 * @param Context	Application context
	 * @param String	Key
	 * @param long		Value
	 * @return boolean	Status of the write
	 */
	public static boolean setLong(final String name, final Context context,
			final String key,
			long value) {
		if (context == null || key == null) return false;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		final SharedPreferences.Editor editor = settings.edit();
		editor.putLong(key, value);
		return editor.commit();
	}
	
	/**
	 * Fetches long by provided key
	 * @param Context	Application context
	 * @param String	Key
	 * @return long		Decrypted value, or Long.MIN_VALUE if error
	 */
	public static long getLong(final String name, final Context context, final String key) {
		if (context == null || key == null) return Long.MIN_VALUE;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		return settings.getLong(key, Long.MIN_VALUE);
	}
	
	/**
	 * Save float to shared preferences
	 * @param Context	Application context
	 * @param String	Key
	 * @param float		Value
	 * @return boolean	Status of the write
	 */
	public static boolean setFloat(final String name,
			final Context context,
			final String key,
			float value) {
		if (context == null || key == null) return false;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		final SharedPreferences.Editor editor = settings.edit();
		editor.putFloat(key, value);
		return editor.commit();
	}
	
	/**
	 * Fetches float by provided key
	 * @param Context	Application context
	 * @param String	Key
	 * @return float	Decrypted value, or Float.MIN_VALUE if error
	 */
	public static float getFloat(final String name, final Context context, final String key) {
		if (context == null || key == null) return Long.MIN_VALUE;
		
		final SharedPreferences settings = context.getSharedPreferences(name, 0);
		return settings.getFloat(key, Float.MIN_VALUE);
	}

}
