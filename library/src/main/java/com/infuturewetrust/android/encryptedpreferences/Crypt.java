package com.infuturewetrust.android.encryptedpreferences;

/*
Copyright 2012 Future Industries Inc.

Author: Artjom Vassiljev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
*/

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

/*
 * Uses AES for encrypting settings
 */

public class Crypt {

	/**
	 * Generates the encryption key 
	 * @param String encryption key
	 * @return byte[] generated key
	 */
	private static byte[] generateKey(final String key) {
		final KeyGenerator kgen;
		final SecureRandom sr;
		try {
			kgen = KeyGenerator.getInstance("AES");
			sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			return null;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		}
		
		sr.setSeed(key.getBytes());
		kgen.init(128, sr);
		return kgen.generateKey().getEncoded();
	}
	
	/**
	 * Encrypts clear text
	 * @param byte[] encryption key
	 * @param byte[] clear text
	 * @return byte[] encrypted text
	 * @throws Exception
	 */
	private static byte[] encrypt(byte[] key, byte[] clear) throws Exception {
        final SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        final Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        return cipher.doFinal(clear);
    }

	/**
	 * Decrypts encrypted text
	 * @param byte[] encryption key
	 * @param byte[] encrypted text
	 * @return byte[] clear text
	 * @throws Exception
	 */
    private static byte[] decrypt(byte[] key, byte[] encrypted) throws Exception {
        final SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        final Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        return cipher.doFinal(encrypted);
    }
	
    /**
     * Encrypts text and returns Base64-encoded string
     * @param String Encryption key
     * @param String Clear text
     * @return String Base64-encoded string or null if error
     */
	public static String encryptString(final String key, final String clear) {
		byte[] kkey = generateKey(key);
		try {
			byte[] text = encrypt(kkey, clear.getBytes());
			return Base64.encodeToString(text, Base64.DEFAULT);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Decrypts provided Base64-encoded string and returns it in clear-text
	 * @param String Encryption key
	 * @param String Encrypted base64-encoded string
	 * @return String Clear text or null if error
	 */
	public static String decryptString(final String key, final String encrypted) {
		byte[] kkey = generateKey(key);
		try {
			byte[] text = decrypt(kkey, Base64.decode(encrypted, Base64.DEFAULT));
			return new String(text);
		} catch (Exception e) {
			return null;
		}
	}

}
